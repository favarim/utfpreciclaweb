import React from 'react';

import Carrusel from '../components/Carrusel.js';

export default function About() {
    return (
        <div className="content">
            <h1 className="title">Sobre o Projeto</h1>
            <h3 className="subtitle">O curso de Montagem e Manutenção de Computadores</h3>
            <div className="paragraph">
                Ofertado para toda a comunidade externa com duas turmas por semestre, tem o objetivo de ensinar o básico em montagem e manutenção de computadores e seus componete.
            </div>
            <Carrusel />
        </div>
    );
}