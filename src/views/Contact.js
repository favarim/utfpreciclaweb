import React from 'react';

import { TextField, Button } from '@material-ui/core';

export default function Contact() {
    return (
        <div className="content">
            <h1 className="title">Contato</h1>
            <h3 className="subtitle">Mande uma mensagem para duvidas, informações ou doações.</h3>
            <div className="paragraph">
                Por favor lembre-se de colocar todas as informações importantes para contato, como e-mail e telefone, se for uma empresa interessada em doar ou receber equipamentos, coloque o nome da empresa junto com as informações de endereço e quantidade de equipamentos a serem doados ou recebidos. Assim que possível entraremos em contato.
            </div>
            <form className="form-control" noValidate autoComplete="off">
                <div className="tab-space"></div>
                <TextField id="form-subject" className="form-imput" label="Assunto" variant="outlined" />
                <div className="tab-space"></div>
                <TextField id="form-text" className="form-imput" label="Texto" multiline rows={10} variant="outlined" />
                <div className="tab-space"></div>
                <Button variant="contained" color="primary">Enviar</Button>
            </form>
            <div className="paragraph">Para mais informações visite o site da <a href="http://www.utfpr.edu.br/campus/patobranco" target="_blank" rel="noopener noreferrer">UTFPR Câmpus Pato Branco</a>. </div>
            <div className="paragraph">Telefone para a universidade pelo número: (46) 3220-2511 ou diretamente para o Departamento de Informática: (46) 3220 2691.</div>
        </div>
    );
}