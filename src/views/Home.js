import React from 'react';

export default function Home() {
    return (
        <div className="content">
            <h1 className="title">Apresentação</h1>
            <div className="paragraph">
                O projeto UTFPRecicla visa atender a comunidade externa da UTFPR Campus Pato Branco, como o objetivo de receber computadores e dispositivos eletrônicos quebrados ou obsoletos doados por empresas que já não utilizam mais esses equipamentos, sendo por meio do projeto recuperados e colocados em funcionalidade em outros lugares que poderão utilizá-los, atendendo a comunidade carente ou repartições publicas podendo ser usados por pessoas que não têm acesso a esses dispositivos e ajudar a informatizar os mesmos. 
                Além disso, o projeto tem o compromisso de semestralmente disponibilizar aulas gratuitas de Montagem e Manutenção de Computadores a comunidade externa, para alunos das mais diversas idades e formações acadêmicas, onde eles apreendem a montar e desmontar computares e os diversos tipos de componentes internos e periféricos de um computadores, como objetivo de no final do curso os alunos possam ser capazes de ter todo o conhecimento práticos e teórico das aulas e aplicar esse conhecimento a nível pessoal ou até para outras pessoas.
            </div>
            <h1 className="title">A UTFPR</h1>
            <div className="paragraph">
                A Universidade Tecnológica Federal do Paraná (UTFPR) é a primeira assim denominada no Brasil e, por isso, tem uma história um pouco diferente das outras universidades. A Instituição não foi criada e, sim, transformada a partir do Centro Federal de Educação Tecnológica do Paraná (Cefet-PR). Como a origem deste centro é a Escola de Aprendizes Artífices, fundada em 1909, a UTFPR herdou uma longa e expressiva trajetória na educação profissional.
                A UTFPR tem como principal foco a graduação, a pós-graduação e a extensão. Oferece 100 cursos superiores de tecnologia, bacharelados (entre eles engenharias) e licenciaturas. Como também atende à necessidade de pessoas que desejam qualificação profissional de nível médio, a UTFPR oferta 19 cursos técnicos em diversas áreas do mercado, como técnicos de nível médio integrado  e cursos técnicos de nível médio subsequentes na modalidade a distância, com polos distribuídos pelos estados  do Paraná e de São Paulo. 
                A consolidação do ensino de graduação incentiva o crescimento da pós-graduação, com a oferta de mais de 90 cursos de especialização, 40 programas de pós-graduação stricto sensu, com cursos de mestrado e doutorado, além de centenas de grupos de pesquisa.
                Na área de relações empresariais e comunitárias, atua fortemente com o segmento empresarial e comunitário, por meio do desenvolvimento de pesquisa aplicada, da cultura empreendedora, de atividades sociais e extraclasse, entre outros.
                Com ampla abrangência no Paraná, a UTFPR tem 13 câmpus no Estado e pretende ampliar essa atuação. Cada Câmpus mantém cursos planejados de acordo com a necessidade da região onde está situado. Uma parte deles oferta cursos técnicos e de graduação, e a maioria somente cursos de graduação e pós-graduação. Todos os cursos de graduação estão autorizados e a grande maioria já foi reconhecida pelo Ministério da Educação.
                A UTFPR tem como missão desenvolver a educação tecnológica de excelência por meio do ensino, pesquisa e extensão, interagindo de forma ética, sustentável, produtiva e inovadora com a comunidade para o avanço do conhecimento e da sociedade. E tem como visão ser modelo educacional de desenvolvimento social e referência na área tecnológica.
            </div>
        </div>
    );
}