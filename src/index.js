import React from 'react'
import ReactDOM from 'react-dom';
import { ThemeProvider, createMuiTheme } from '@material-ui/core';

import App from './views/App.js';

const theme = createMuiTheme({
    "palette":{
        "common":{"black":"#000","white":"#fff"},
        "background":{"paper":"rgba(255, 255, 255, 1)","default":"#fafafa"},
        "primary":{"light":"rgba(89, 211, 47, 1)","main":"rgba(78, 158, 19, 1)","dark":"rgba(5, 70, 0, 1)","contrastText":"#fff"},
        "secondary":{"light":"rgba(255, 200, 74, 1)","main":"rgba(245, 145, 0, 1)","dark":"rgba(201, 105, 0, 1)","contrastText":"#fff"},
        "error":{"light":"#e57373","main":"#f44336","dark":"#d32f2f","contrastText":"#fff"},
        "text":{"primary":"rgba(0, 0, 0, 0.87)","secondary":"rgba(0, 0, 0, 0.54)","disabled":"rgba(0, 0, 0, 0.38)","hint":"rgba(0, 0, 0, 0.38)"}
    }
});

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <App />
    </ThemeProvider>,
    document.getElementById('root')
)
