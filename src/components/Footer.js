import React from 'react';

import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { Box } from '@material-ui/core';

export default function Footer() {
    return (
        <Box className="footer" bgcolor="primary.main">
            <Container maxWidth="xs">
                <Typography variant="body1">Projeto UTFPRecicla - UTFPR Câmpus Pato Branco</Typography>
                <Typography variant="body2">&copy; Todos direitos reservados 2020</Typography>
            </Container>
        </Box>
    );
}