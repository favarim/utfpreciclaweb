import React from 'react';
import Typography from '@material-ui/core/Typography';

export default function Header() {
    return (
        <div className="header">
            <img src={require("../assets/img/motherboard.jpg")} alt="UTFPRecicla" />
            <Typography className="name" variant="h4" color="primary"><h1>UTFPRecicla</h1></Typography>
        </div>
    );
}