import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';

import '../assets/css/style.css'

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const tutorialSteps = [
  { label: 'img1', imgPath: require('../assets/img/carrusel-img1.jpg?auto=format&fit=crop&w=400&h=250&q=60'), },
  { label: 'img2', imgPath: require('../assets/img/carrusel-img2.jpg?auto=format&fit=crop&w=400&h=250&q=60'), },
  { label: 'img3', imgPath: require('../assets/img/carrusel-img3.jpg?auto=format&fit=crop&w=400&h=250&q=80'), },
  { label: 'img4', imgPath: require('../assets/img/carrusel-img4.jpg?auto=format&fit=crop&w=400&h=250&q=60'), },
  { label: 'img5', imgPath: require('../assets/img/carrusel-img5.jpg?auto=format&fit=crop&w=400&h=250&q=60'), },
];

export default function Carrusel() {
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = tutorialSteps.length;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <div className="carrusel">
      <AutoPlaySwipeableViews axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'} index={activeStep} onChangeIndex={handleStepChange} enableMouseEvents >
        {tutorialSteps.map((step, index) => (
          <div key={step.label}>
            {Math.abs(activeStep - index) <= 2 ? (
              <img className="carrusel-img" src={step.imgPath} alt={step.label} />
            ) : null}
          </div>
        ))}
      </AutoPlaySwipeableViews>
      <MobileStepper variant="dots" steps={maxSteps} position="static" activeStep={activeStep}
      nextButton={
        <Button size="small" onClick={handleNext} disabled={activeStep === 4}> <NavigateNextIcon /> </Button>
      }
      backButton={
        <Button size="small" onClick={handleBack} disabled={activeStep === 0}> <NavigateBeforeIcon /> </Button>
      } />
    </div>
  );
}
