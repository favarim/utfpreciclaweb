import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function NavBar() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <a href="http://www.utfpr.edu.br/campus/patobranco" target="_blank" rel="noopener noreferrer"><img src={require("../assets/img/utfpr-logo.png")} alt="Site UTFPR"></img></a>
                    <Typography variant="h6" className={classes.title}></Typography>
                    <a href="http://patobranco.pr.gov.br" target="_blank" rel="noopener noreferrer"><img src={require("../assets/img/pato-branco-logo.png")} alt="Prefeitura de Pato Branco"></img></a>
                </Toolbar>
            </AppBar>
        </div>
    );
}
