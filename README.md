#  Montagem e Manutenção de Computadores

#### *Projeto para o desenvolvimento do site de apresentação do curso*

#### *Atualize esse arquivo para colocar links e informação sobre React, Git e Markdown*

#### *Prazo até o final do mês de Junho (2020) para aprender React*

#### *Acompanhe as issues para ver as atividade a fazer*

### Alguns links importantes:
- [O básico de Markdown](https://www.markdownguide.org/basic-syntax)
- [Meu resumo de git](https://github.com/FelipeColtri/Git)
- [Mais um pouco de git](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)

